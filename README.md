# About ME
## _Hello!!!!_

Konnichiwa! My name is Tareena —also known as **Ray**. I am from Kedah. I like to read mangas, binge netflix shows and cafe hopping!

![me](https://gitlab.com/tareenanazmin/self-introduction/uploads/def7b0c8f84c6007389a0de492f80b68/IMG_20171012_105650.jpg)

### ✨Strength
- honest
- enthusiastic
- supportive
- friendly

### ✨Weakness
- overtalk
- procastinate
- overthink
- feeling-based

## Features

| Plugin | Link |
| ------ | ------ |
| Spotify | [<img src="https://campaigns.scdn.co/images/spotify.png" alt="rayariffin" width="100"/>](https://open.spotify.com/user/rayariffin?si=57110daf491f4a66) |
| Instagram | [<img src="https://cdn-icons-png.flaticon.com/512/174/174855.png" alt="rayariffin" width="30"/>](https://www.instagram.com/rayariffin) |

>よろしく! :raised_hand:
